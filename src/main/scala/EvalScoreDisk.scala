import java.io.{File, FileWriter}
import java.text.DecimalFormat
import java.util._

import com.github.tototoshi.csv.CSVReader
import com.google.common.collect.{ArrayListMultimap, Multimap}

import scala.collection.JavaConversions._ //use scala-arm from http://jsuereth.com/scala-arm/

object EvalScoreDisk {
  def main(args: Array[String]): Unit = {
    val startTime: Long = System.nanoTime()

     var reportViewCSVFile: String = ""
     var deafaulCSVFile: String = ""
     var demeaCSVFile: String = ""
     var reportScoreFile: String = ""

    //input file names
    reportViewCSVFile = "D:\\Vinay\\sivasankaran_veerabaku-report-similarity-216958ab9eac\\data\\Views_IN_TABLEAU (3).csv"
   deafaulCSVFile = "D:\\Vinay\\sivasankaran_veerabaku-report-similarity-216958ab9eac\\data\\Columns_IN_Default.csv"
    //demeaCSVFile = "D:\\Vinay\\sivasankaran_veerabaku-report-similarity-216958ab9eac\\data\\Columns_IN_DEMEA.csv"
    reportScoreFile = "D:\\Vinay\\data\\reportidScore.csv"

    //defining multimaps to allow duplicate keys
    val defaultCSVMultiMap: Multimap[String, String] = ArrayListMultimap.create()
    val demeaCSVMultiMap: Multimap[String, String] = ArrayListMultimap.create()

    //reading input files
    try {
      val defaultReader = CSVReader.open(new File(deafaulCSVFile))
      defaultReader.readNext()
      defaultReader.foreach { cols => {
        if (cols.length > 1) {
          defaultCSVMultiMap.put(cols(0).trim, cols(3).trim + "," + cols(4).trim + "," + cols(5).trim + "," + cols(6).trim() + "," + cols(8).trim + "," + cols(9).trim)
        }
      }
      }
    }
    catch{
      case e: Exception =>
    }

    try{
      val demeatReader = CSVReader.open(new File(demeaCSVFile))
      demeatReader.readNext()
      demeatReader.foreach { cols => {
        if (cols.length > 1) {
          demeaCSVMultiMap.put(cols(0).trim, cols(3).trim + "," + cols(4).trim + "," + cols(5).trim + "," + cols(6).trim() + "," + cols(8).trim + "," + cols(9).trim)
        }
      }
      }
    }
    catch{
      case e: Exception =>
    }

    //defining maps to store group by values with unique key
    val defaultCSVMap: Map[String, Collection[String]] = defaultCSVMultiMap.asMap()
    val demeaCSVMap: Map[String, Collection[String]] = demeaCSVMultiMap.asMap()

    //final report map to store default and demea csv map data
    val finalReportMap1: Map[String, Collection[String]] = new HashMap[String, Collection[String]]()

    //storing default and demea data into one map
    try{
    val viewReader = CSVReader.open(new File(reportViewCSVFile))
    viewReader.readNext()
    viewReader.foreach { cols => {
      val viewReportId: String = cols(3).trim
      for (defaultColumnsReportId <- defaultCSVMap.keySet) {
        if (viewReportId.equals(defaultColumnsReportId.trim))
          finalReportMap1.put(viewReportId, defaultCSVMap.get(viewReportId))
      }
      for (demeaColumnsReportId <- demeaCSVMap.keySet) {
        if (viewReportId.equals(demeaColumnsReportId.trim))
          finalReportMap1.put(viewReportId, demeaCSVMap.get(viewReportId))
      }
    }
    }
    }
    catch{
      case e: Exception =>
    }

    //duplicating finalreport2 same as finalreport1
    val finalReportMap2: Map[String, Collection[String]] = new HashMap[String, Collection[String]]()
    finalReportMap2.putAll(finalReportMap1)

    val df: DecimalFormat = new DecimalFormat("####0.0")
    val myWriter: FileWriter = new FileWriter(reportScoreFile, false)
    var totalCount: Long = 0

    // Logic to find duplicate reports - Brute force comparision
    for (temp1ReportId <- finalReportMap1.keySet) {
      val temp1Value: Collection[String] = finalReportMap1.get(temp1ReportId)
      finalReportMap2.remove(temp1ReportId)
      for (temp2ReportId <- finalReportMap2.keySet) {
        val temp2Value: Collection[String] = finalReportMap1.get(temp2ReportId)
        // use the copy constructor
        val intersection: Set[String] = new HashSet[String](temp1Value)
        intersection.retainAll(temp2Value)
        val intersectionScore: Int = intersection.size
        val finalScore: java.lang.Float = java.lang.Float.parseFloat(
          df.format(intersectionScore.toDouble / Math.max(temp1Value.size,
            temp2Value.size)))
        if (finalScore >= 0.8) {
          myWriter.write(temp1ReportId + "," + temp2ReportId + "," + finalScore)
          myWriter.write(System.getProperty("line.separator"))
        }
        totalCount += 1
      }

    }

    //closing the writer
    myWriter.close()

    //evaluating no of lines of score file
    val scoreCount = io.Source.fromFile(reportScoreFile).getLines.size

    println("finalReportMap1:" + finalReportMap1.size())
    println("finalReportMap2:" + finalReportMap2.size())
    println("-------------------------------------------")
    println("default report size: " + defaultCSVMap.size)
    println("demea report size: " + demeaCSVMap.size)
    println("total report size (n):" + (defaultCSVMap.size + demeaCSVMap.size))
    println("total multi-multi count  n (n-1)/2: " + totalCount)
    println("match  size: " + scoreCount)
  }
}