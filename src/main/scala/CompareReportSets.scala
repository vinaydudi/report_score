
import java.io.{File, FileWriter}

import com.github.tototoshi.csv.CSVReader

import scala.util.control.Breaks._

object CompareReportsSetsTest {

  def main(args: Array[String]): Unit = {

    //input files locations
    val report1File: String = "D:\\Vinay\\data\\reports_score1.csv"
    val report2File: String = "D:\\Vinay\\data\\tmp_pairs_90pct.csv"

    //output files locations
    val common1: String = "D:\\Vinay\\data\\intersection1.csv"
    val r1_minus_r2: String = "D:\\Vinay\\data\\r1_minus_r2.csv"
    val r2_minus_r1: String = "D:\\Vinay\\data\\r2_minus_r1.csv"

    var report1Set = scala.collection.mutable.Set[String]()
    var report2Set = scala.collection.mutable.Set[String]()

    //reading input files
    val bufferedSource1 = CSVReader.open(new File(report1File))
    bufferedSource1.foreach { cols => {
      report1Set += cols(0).trim + "," + cols(1).trim
    }
    }
    val bufferedSource2 = CSVReader.open(new File(report2File))
    bufferedSource2.foreach { cols => {
      report2Set += cols(0).trim + "," + cols(1).trim
    }
    }

    val nestedSet1 = new scala.collection.mutable.HashSet[scala.collection.mutable.Set[String]]()
    val nestedSet2 = new scala.collection.mutable.HashSet[scala.collection.mutable.Set[String]]()

    //storing distinct reports in set
    report1Set.foreach(s1 => {
      var set1 = scala.collection.mutable.Set[String]()

      s1.split(",").foreach(ss1 => {
        set1 += ss1
      })
      nestedSet1 += set1
    })
    report2Set.foreach(s1 => {
      var set1 = scala.collection.mutable.Set[String]()
      s1.split(",").foreach(ss1 => {
        set1 += ss1
      })
      nestedSet2 += set1
    })

    println("r1 count: " + report1Set.size)
    println("r2 count: " + report2Set.size)

   //intermediate variables declaration
    var commonCount1: Long = 0
    var commonCount2: Long = 0

    var r1_minus_r2_count: Long = 0
    var r2_minus_r1_count: Long = 0

    val commonWriter1: FileWriter = new FileWriter(common1, false)
    val r1_minus_r2_writer: FileWriter = new FileWriter(r1_minus_r2, false)
    val r2_minus_r1_writer: FileWriter = new FileWriter(r2_minus_r1, false)

    var flag1: Boolean = false
    var flag2: Boolean = false

    //finding intersection and r1-r2 of two files
    nestedSet1.foreach(s1 => {
      flag1 = false
      breakable {
        nestedSet2.foreach(s2 => {
          if (s1 == s2) {
            commonWriter1.write(s1.toString() + System.lineSeparator())
            commonCount1 += 1
            flag1 = true
            break
          }

        })
      }
      if (flag1 == false) {
        r1_minus_r2_writer.write(s1.toString() + System.lineSeparator())
        r1_minus_r2_count += 1
      }
    })

    println("common count: " + commonCount1)
    println("r1-r2 count: " + r1_minus_r2_count)

    //finding intersection and r2-r1 of two files
    nestedSet2.foreach(s1 => {
      flag2 = false
      breakable {
        nestedSet1.foreach(s2 => {
          if (s1 == s2) {
            commonCount2 += 1
            flag2 = true
            break
          }
        })
      }
      if (flag2 == false) {
        r2_minus_r1_writer.write(s1.toString() + System.lineSeparator())
        r2_minus_r1_count += 1
      }
    })

    println("common count: " + commonCount2)
    println("r2-r1 count: " + r2_minus_r1_count)
  }
}
