import java.io.{File, FileWriter}
import java.text.DecimalFormat
import java.util._

import com.github.tototoshi.csv.CSVReader
import com.google.common.collect.{ArrayListMultimap, Multimap}

import scala.collection.JavaConversions._ //use scala-arm from http://jsuereth.com/scala-arm/

object MatchColumns {
  def main(args: Array[String]): Unit = {
    val startTime: Long = System.nanoTime()

     var reportViewCSVFile: String = ""
     var deafaulCSVFile: String = ""
     var demeaCSVFile: String = ""
     var reportScoreFile: String = ""

    //input file names
    reportViewCSVFile = "D:\\Vinay\\sivasankaran_veerabaku-report-similarity-216958ab9eac\\data\\Views_IN_TABLEAU (3).csv"
   deafaulCSVFile = "D:\\Vinay\\sivasankaran_veerabaku-report-similarity-216958ab9eac\\data\\Columns_IN_Default.csv"
    demeaCSVFile = "D:\\Vinay\\sivasankaran_veerabaku-report-similarity-216958ab9eac\\data\\Columns_IN_DEMEA.csv"
    reportScoreFile = "D:\\Vinay\\data\\repots_with_columns.csv"

    //defining multimaps to allow duplicate keys
    val defaultCSVMultiMap: Multimap[String, String] = ArrayListMultimap.create()
    val demeaCSVMultiMap: Multimap[String, String] = ArrayListMultimap.create()

    //reading input files
    try {
      val defaultReader = CSVReader.open(new File(deafaulCSVFile))
      defaultReader.readNext()
      defaultReader.foreach { cols => {
        if (cols.length > 1)
          defaultCSVMultiMap.put(cols(0).trim, cols(8).trim)
      }
      }
    }
    catch{
      case e: Exception =>
    }

    try{
      val demeatReader = CSVReader.open(new File(demeaCSVFile))
      demeatReader.readNext()
      demeatReader.foreach { cols => {
        if (cols.length > 1) {
          demeaCSVMultiMap.put(cols(0).trim, cols(8).trim )
        }
      }
      }
    }
    catch{
      case e: Exception =>
    }



    //defining maps to store group by values with unique key
    val defaultCSVMap: Map[String, Collection[String]] = defaultCSVMultiMap.asMap()
    val demeaCSVMap: Map[String, Collection[String]] = demeaCSVMultiMap.asMap()



    //final report map to store default and demea csv map data
    val finalReportMap1: Map[String, Collection[String]] = new HashMap[String, Collection[String]]()

    //storing default and demea data into one map
    var count = 0

    try{
    val viewReader = CSVReader.open(new File(reportViewCSVFile))
    viewReader.readNext()
    viewReader.foreach { cols => {
      val dashboardName: String = cols(2).trim
      val viewReportId: String = cols(3).trim
      val reportName: String = cols(4).trim

      val reportCombo = dashboardName+","+viewReportId+","+reportName
      for (defaultColumnsReportId <- defaultCSVMap.keySet) {
        if (viewReportId.equals(defaultColumnsReportId.trim)) {
          finalReportMap1.put(reportCombo, defaultCSVMap.get(viewReportId))
        }
      }
      for (demeaColumnsReportId <- demeaCSVMap.keySet) {
        if (viewReportId.equals(demeaColumnsReportId.trim)) {
          finalReportMap1.put(reportCombo, demeaCSVMap.get(viewReportId))
        }

      }
    }
    }
    }
    catch{
      case e: Exception =>
    }

    val myWriter: FileWriter = new FileWriter(reportScoreFile, false)
    for(reportId <- finalReportMap1.keySet()){
      val values = finalReportMap1.get(reportId)
      val row = reportId+","+values.toString.replace("[","").replace("]",",").dropRight(1)

      count +=1

     myWriter.write(row)
      myWriter.write(System.getProperty("line.separator"))
    }
    println(count)
  }
}