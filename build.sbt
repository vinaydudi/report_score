name := "report_score_evaluation"

version := "0.1"

scalaVersion := "2.11.11"

// https://mvnrepository.com/artifact/au.com.bytecode/opencsv
libraryDependencies += "au.com.bytecode" % "opencsv" % "2.4"

// https://mvnrepository.com/artifact/com.google.guava/guava
libraryDependencies += "com.google.guava" % "guava" % "r05"

libraryDependencies += "com.github.tototoshi" %% "scala-csv" % "1.3.6"
